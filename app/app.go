package app

import (
	"errors"
	"fmt"
	"github.com/TeplyyMaksim/alphazone_backend/constants"
	"github.com/TeplyyMaksim/alphazone_backend/ctrl"
	authService "github.com/TeplyyMaksim/alphazone_backend/service/auth"
	"github.com/TeplyyMaksim/alphazone_backend/utils"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"net/http"
	"strings"
)

var router = echo.New()

var authMiddleware = middleware.KeyAuthWithConfig(
	middleware.KeyAuthConfig{
		KeyLookup: "header:authorization",
		Validator: func(token string, c echo.Context) (bool, error) {
		token = strings.TrimSpace(strings.ReplaceAll(token, "bearer", ""))

		user, err := authService.GetUser(token)

		if err != nil {
		return false, errors.New("Auth failed")
		}

		c.Set(constants.ContextUserKey, user)
		c.Set(constants.ContextTokenKey, token)

		return true, nil
	},
})

func StartApplication() {
	// Auth
	auth := router.Group("/auth")
	auth.POST("/sign-up", ctrl.SignUp)
	auth.POST("/login", ctrl.Login)

	// Product Public
	product := router.Group("/product")
	product.GET("/list", ctrl.GetProductList)

	// User product
	userProduct := router.Group("/user-product")
	userProduct.Use(authMiddleware)
	userProduct.GET("", ctrl.GetUserProductList)
	userProduct.POST("", ctrl.CreateProduct)
	userProduct.PUT("", ctrl.UpdateProduct)
	userProduct.DELETE("/:id", ctrl.DeleteProduct)
	userProduct.POST("/favorite-list/add", ctrl.FavoriteProduct)
	userProduct.POST("/favorite-list/remove", ctrl.UnavoriteProduct)
	userProduct.GET("/favorite-list", ctrl.GetFavoriteProductIdList)

	// Order
	order := router.Group("/order")
	order.Use(authMiddleware)
	order.POST("", ctrl.CreateOrder)
	order.GET("/list", ctrl.GetOrderList)


	//Logout
	logout := router.Group("/logout")
	logout.Use(authMiddleware)
	logout.POST("", ctrl.Logout)

	// Middlewares
	router.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			err := next(c)
			if nil != err {
				switch err.(type) {
				case utils.Error:
					maxError := err.(utils.Error)
					return c.JSON(maxError.StatusCode, maxError)
				default:
					if strings.HasPrefix(err.Error(), "code=401") {
						return c.JSON(
							http.StatusUnauthorized,
							utils.NewError(
								http.StatusUnauthorized,
								"Invalid token",
								"invalid_token_error",
							),
						)
					}

					if strings.HasPrefix(err.Error(), "code=404") {
						return c.JSON(
							http.StatusNotFound,
							utils.NewError(
								http.StatusNotFound,
								fmt.Sprintf("Path %s not found", c.Path()),
								"not_found_error",
							),
						)
					}

					return c.JSON(http.StatusBadRequest, utils.NewError(http.StatusBadRequest, err.Error(), "BAD_REQUEST_ERROR"))
				}
			}
			return nil
		}
	})

	err  := router.Start(":8888")
	if err != nil {
		panic(err)
	}
}
