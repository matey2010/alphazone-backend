package ctrl

import (
	"github.com/TeplyyMaksim/alphazone_backend/dto"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"github.com/TeplyyMaksim/alphazone_backend/service/auth"
	"github.com/TeplyyMaksim/alphazone_backend/utils"
	"github.com/labstack/echo/v4"
	"net/http"
)

func SignUp (ctx echo.Context) error {
	request := dto.SignUpRequest{}
	err := ctx.Bind(&request)

	if err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 400,
			Code:       "sign_up_bad_request_error",
		}
	}

	user := &model.User{
		Email:    request.Email,
		Password: request.Password,
	}

	if err = auth.SignUp(user); err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewSignUpResponse())
}

func Login (ctx echo.Context) error {
	request := dto.LoginRequest{}
	err := ctx.Bind(&request)

	if err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 400,
			Code:       "sign_in_bad_request_error",
		}
	}

	session, err := auth.Login(request.Email, request.Password)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewLoginResponse(session))
}

func Logout (ctx echo.Context) error {
	token, err := utils.GetTokenFromEchoContext(ctx)
	if err != nil {
		return err
	}

	if err = auth.Logout(token); err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.LogoutResponse{})
}
