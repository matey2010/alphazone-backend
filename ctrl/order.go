package ctrl

import (
	"github.com/TeplyyMaksim/alphazone_backend/dto"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"github.com/TeplyyMaksim/alphazone_backend/service/order"
	"github.com/TeplyyMaksim/alphazone_backend/utils"
	"github.com/labstack/echo/v4"
	"net/http"
	"time"
)

func CreateOrder (ctx echo.Context) error {
	request := dto.CreateOrderRequest{}
	err := ctx.Bind(&request)

	if err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 400,
			Code:       "create_order_bad_request_error",
		}
	}

	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return err
	}

	newOrder := &model.Order{
		CreatedAt:     time.Now().Unix(),
		OrderItemList: make(model.OrderItemList, len(request.OrderItemList)),
		UserId: user.Id,
	}

	for i, itemRequest := range request.OrderItemList {
		newOrder.OrderItemList[i] = model.OrderItem{
			Quantity:  itemRequest.Quantity,
			ProductId: itemRequest.ProductId,
		}
	}

	if err = order.CreateOrder(newOrder); err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewOrderResponse(newOrder))
}

func GetOrderList (ctx echo.Context) error {
	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return err
	}

	list, err := order.GetOrderList(user)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewOrderListResponse(list))
}
