package ctrl

import (
	"github.com/TeplyyMaksim/alphazone_backend/dto"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"github.com/TeplyyMaksim/alphazone_backend/service/product"
	"github.com/TeplyyMaksim/alphazone_backend/utils"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

func CreateProduct(ctx echo.Context) error {
	request := dto.CreateProductRequest{}
	err := ctx.Bind(&request)
	if err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 400,
			Code:       "create_product_bad_request_error",
		}
	}

	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return err
	}

	newProduct := &model.Product{
		Title:       request.Title,
		Description: request.Description,
		Price:       request.Price,
		ImageUrl:    request.ImageUrl,
		CreatorId:   user.Id,
	}

	if err = product.CreateProduct(newProduct); err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewProductResponse(newProduct))
}

func GetProductList(ctx echo.Context) error {
	list, err := product.GetProductList()

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewProductListResponse(list))
}

func GetUserProductList(ctx echo.Context) error {
	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return err
	}

	list, err := product.GetUserProductList(user)

	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewProductListResponse(list))
}

func UpdateProduct(ctx echo.Context) error {
	request := dto.UpdateProductRequest{}
	err := ctx.Bind(&request)
	if err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 400,
			Code:       "update_product_bad_request_error",
		}
	}

	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return err
	}

	prod := &model.Product{
		Id:          request.Id,
		Title:       request.Title,
		Description: request.Description,
		Price:       request.Price,
		ImageUrl:    request.ImageUrl,
		CreatorId:   user.Id,
	}

	if err = product.UpdateProduct(prod); err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewProductResponse(prod))
}

func DeleteProduct(ctx echo.Context) error {
	id, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		return utils.NewError(http.StatusBadRequest, "Bad product id", "bad_product_id_error")
	}

	err = product.DeleteProduct(id)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.DeleteProductResponse{})
}

func GetFavoriteProductIdList(ctx echo.Context) error {
	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return err
	}

	favoriteProductList, err := product.GetFavoriteProductList(user)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.NewGetFavoriteProductIdListResponse(favoriteProductList))
}

func FavoriteProduct(ctx echo.Context) error {
	request := dto.FavoriteProductRequest{}
	err := ctx.Bind(&request)
	if err != nil {
		return err
	}

	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 400,
			Code:       "favorite_product_bad_request_error",
		}
	}

	if err = product.FavoriteProduct(user, request.Id); err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.FavoriteProductResponse{})
}

func UnavoriteProduct(ctx echo.Context) error {
	request := dto.FavoriteProductRequest{}
	err := ctx.Bind(&request)
	if err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 400,
			Code:       "unfavorite_product_bad_request_error",
		}
	}

	user, err := utils.GetUserFromEchoContext(ctx)
	if err != nil {
		return err
	}

	if err = product.UnfavoriteProduct(user, request.Id); err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, dto.FavoriteProductResponse{})
}
