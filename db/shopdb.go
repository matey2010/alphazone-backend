package db

import (
	"fmt"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var ShopDb *gorm.DB

func Run() {
	dsn := fmt.Sprintf(
		"host=%s user=%s password='%s' dbname=%s port=%s sslmode=disable",
		"localhost",
		"maksymteplyy",
		"",
		"alphazone",
		"5432",
	)

	var err error
	ShopDb, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic(err)
	}

	if err = ShopDb.AutoMigrate(
		// Product and Order
		model.Product{},
		model.Order{},
		model.OrderItem{},

		// User and Auth
		model.User{},
		model.Session{},

		// User Product
		model.UserFavoriteProduct{},
	); err != nil {
		panic(err)
	}
}
