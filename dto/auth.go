package dto

import "github.com/TeplyyMaksim/alphazone_backend/model"

type SignUpRequest struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

type SignUpResponse struct {
	Status string `json:"status"`
}

func NewSignUpResponse() SignUpResponse {
	return SignUpResponse{ Status: "ok" }
}

type LoginRequest struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Token string `json:"token"`
	ExpiresAt int64 `json:"expires_at"`
}

func NewLoginResponse(session *model.Session) LoginResponse {
	return LoginResponse{
		Token: session.Token,
		ExpiresAt: session.ExpiresAt,
	}
}

type LogoutResponse struct {}
