package dto

import (
	"github.com/TeplyyMaksim/alphazone_backend/model"
)

type OrderItemResponse struct {
	Id        int             `json:"id"`
	ProductId int             `json:"product_id"`
	Product   ProductResponse `json:"product"`
	Quantity  int             `json:"quantity"`
}

func NewOrderItemResponse(item *model.OrderItem) OrderItemResponse {
	return OrderItemResponse{
		Id:        item.Id,
		ProductId: item.ProductId,
		Product:   NewProductResponse(&item.Product),
		Quantity:  item.Quantity,
	}
}

type OrderItemListResponse []OrderItemResponse

type OrderResponse struct {
	Id            int                   `json:"id"`
	Amount        float32               `json:"amount"`
	DateTime      int64                 `json:"date_time"`
	OrderItemList OrderItemListResponse `json:"items"`
}

func NewOrderResponse(order *model.Order) OrderResponse {
	items := make(OrderItemListResponse, len(order.OrderItemList))
	var amount float32 = 0

	for i, item := range order.OrderItemList {
		items[i] = NewOrderItemResponse(&item)
		amount += item.Product.Price * float32(item.Quantity)
	}

	return OrderResponse{
		Id:            order.Id,
		Amount:        amount,
		DateTime:      order.CreatedAt,
		OrderItemList: items,
	}
}

type OrderItemRequest struct {
	Quantity  int `json:"quantity"`
	ProductId int `json:"product_id"`
}

type OrderItemListRequest []OrderItemRequest

type CreateOrderRequest struct {
	OrderItemList OrderItemListRequest `json:"items"`
}

type OrderListResponse []OrderResponse

func NewOrderListResponse(orderList model.OrderList) OrderListResponse {
	list := OrderListResponse{}

	for _, order := range orderList {
		list = append(list, NewOrderResponse(&order))
	}

	return list
}
