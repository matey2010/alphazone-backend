package dto

import (
	"github.com/TeplyyMaksim/alphazone_backend/model"
)

type CreateProductRequest struct {
	Title string `json:"title"`
	Description string `json:"description"`
	Price float32 `json:"price"`
	ImageUrl string `json:"image_url"`
}

type ProductResponse struct {
	Id int `json:"id"`
	Title string `json:"title"`
	Description string `json:"description"`
	Price float32 `json:"price"`
	ImageUrl string `json:"image_url"`
}

func NewProductResponse(product *model.Product) ProductResponse {
	return ProductResponse{
		Id:          product.Id,
		Title:       product.Title,
		Description: product.Description,
		Price:       product.Price,
		ImageUrl:    product.ImageUrl,
	}
}

type ProductListResponse []ProductResponse

func NewProductListResponse(productList model.ProductList) ProductListResponse {
	list := ProductListResponse{}

	for _, product := range productList {
		list = append(list, NewProductResponse(&product))
	}

	return list
}

type UpdateProductRequest struct {
	Id int `json:"id"`
	Title string `json:"title"`
	Description string `json:"description"`
	Price float32 `json:"price"`
	ImageUrl string `json:"image_url"`
}

type DeleteProductResponse struct {}

func NewGetFavoriteProductIdListResponse (favoriteProductList model.UserFavoriteProductList) []int {
	resp := make([]int, len(favoriteProductList))

	for i, fav := range favoriteProductList {
		resp[i] = fav.ProductId
	}

	return resp
}

type FavoriteProductRequest struct {
	Id int `json:"id"`
}

type FavoriteProductResponse struct {}
