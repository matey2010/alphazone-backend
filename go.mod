module github.com/TeplyyMaksim/alphazone_backend

go 1.15

require (
	github.com/labstack/echo/v4 v4.3.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.10
)
