package main

import (
	"github.com/TeplyyMaksim/alphazone_backend/app"
	"github.com/TeplyyMaksim/alphazone_backend/db"
)

func main() {
	db.Run()
	app.StartApplication()
}
