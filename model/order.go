package model

type OrderItem struct {
	Id int `json:"id" gorm:"primaryKey"`
	Quantity int `json:"quantity"`
	ProductId int `json:"product_id"`
	Product Product `gorm:"->;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
	OrderId int `json:"order_id"`
	Order Order `gorm:"->;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
}

func (OrderItem) TableName() string {
	return "order_item"
}

type OrderItemList []OrderItem

type Order struct {
	Id int `json:"id" gorm:"primaryKey"`
	CreatedAt int64 `json:"created_at"`
	OrderItemList OrderItemList `gorm:"->;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
	UserId int `json:"user_id"`
	User User `gorm:"->;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
}

func (Order) TableName() string {
	return "order"
}

type OrderList []Order
