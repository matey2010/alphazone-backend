package model

type Product struct {
	Id int `json:"id" gorm:"primaryKey"`
	Title string `json:"title"`
	Description string `json:"description"`
	Price float32 `json:"price"`
	ImageUrl string `json:"image_url"`
	CreatorId int `json:"creator_id"`
	Creator User `gorm:"->;foreignKey:CreatorId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
}

func (Product) TableName() string {
	return "product"
}

type ProductList []Product
