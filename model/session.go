package model

type Session struct {
	Token string `json:"token" gorm:"primaryKey"`
	UserId int `json:"user_id"`
	User User `gorm:"->;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
	CreatedAt int64 `json:"created_at"`
	ExpiresAt int64 `json:"expires_at"`
}

func (Session) TableName() string {
	return "session"
}
