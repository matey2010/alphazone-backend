package model

type User struct {
	Id int `json:"id" gorm:"primaryKey"`
	Email string `json:"email" gorm:"uniqueIndex:email_unique"`
	Password string `json:"password"`
}

func (User) TableName() string {
	return "user"
}
