package model

type UserFavoriteProduct struct {
	Id int `json:"id" gorm:"primaryKey"`
	UserId int `gorm:"uniqueIndex:user_product_unique" json:"user_id"`
	User User `gorm:"->;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
	ProductId int `gorm:"uniqueIndex:user_product_unique" json:"product_id"`
	Product Product `gorm:"->;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;" json:"-"`
}

func (UserFavoriteProduct) TableName() string {
	return "user_favorite_product"
}

type UserFavoriteProductList []UserFavoriteProduct


