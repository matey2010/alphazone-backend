package auth

import (
	"github.com/TeplyyMaksim/alphazone_backend/db"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"github.com/TeplyyMaksim/alphazone_backend/utils"
	"gorm.io/gorm"
	"net/http"
	"time"
)

const expirationSeconds = (time.Minute * 5) / time.Second

func SignUp(user *model.User) error {
	if err := db.ShopDb.Create(user).Error; err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: http.StatusInternalServerError,
			Code:       "sign_up_error",
		}
	}

	return nil
}

func Login(email string, password string) (*model.Session, error) {
	session := &model.Session{}

	if err := db.ShopDb.Transaction(func(tx *gorm.DB) error {
		user := &model.User{}

		if err := tx.Where("email = ? AND password = ?", email, password).First(user).Error; err != nil {
			return utils.Error{
				Message:    "Wrong email or password",
				StatusCode: http.StatusBadRequest,
				Code:       "login_error",
			}
		}

		currentTime := time.Now().Unix()

		session.Token = utils.GenerateToken(10)
		session.UserId = user.Id
		session.CreatedAt = currentTime
		session.ExpiresAt = currentTime + int64(expirationSeconds)

		if err := db.ShopDb.Create(session).Error; err != nil {
			return utils.Error{
				Message:    "Can't create session",
				StatusCode: http.StatusInternalServerError,
				Code:       "create_session_error",
			}
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return session, nil
}

func Logout(token string) error {
	if err := db.ShopDb.Where("token = ?", token).Delete(&model.Session{}).Error; err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: http.StatusInternalServerError,
			Code:       "logout_error",
		}
	}

	return nil
}

func GetUser(token string) (*model.User, error) {
	user := &model.User{}

	if err := db.ShopDb.Find(user).
		Joins(
			"INNER JOIN \"session\" ON \"session\".\"user_id\" = \"user\".\"id\" "+
				"AND \"session\".\"token\" = ? "+
				"AND \"session\".\"expires_at\" > ?",
			token,
			time.Now().Unix(),
		).
		First(user).Error; err != nil {
		return nil, utils.Error{
			Message:    err.Error(),
			StatusCode: http.StatusInternalServerError,
			Code:       "get_user_error",
		}
	}

	return user, nil
}
