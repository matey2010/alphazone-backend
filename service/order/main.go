package order

import (
	"github.com/TeplyyMaksim/alphazone_backend/db"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"github.com/TeplyyMaksim/alphazone_backend/utils"
	"gorm.io/gorm"
	"net/http"
)

func CreateOrder(order *model.Order) error {
	if err := db.ShopDb.Transaction(func(tx *gorm.DB) error {
		if err := tx.Create(order).Error; err != nil {
			return utils.Error{
				Message:    err.Error(),
				StatusCode: http.StatusInternalServerError,
				Code:       "create_order_error",
			}
		}

		for index := range order.OrderItemList {
			// Create order item
			order.OrderItemList[index].OrderId = order.Id
			if err := tx.Create(&order.OrderItemList[index]).Error; err != nil {
				return utils.Error{
					Message:    err.Error(),
					StatusCode: http.StatusInternalServerError,
					Code:       "create_order_item_error",
				}
			}

			// Get product and insert it into order item above
			if err := tx.First(&order.OrderItemList[index].Product, order.OrderItemList[index].ProductId).Error; err != nil {
				return utils.Error{
					Message:    err.Error(),
					StatusCode: http.StatusInternalServerError,
					Code:       "get_product_error",
				}
			}
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

func GetOrderList (user *model.User) (model.OrderList, error) {
	list := model.OrderList{}

	if err := db.ShopDb.Preload("OrderItemList.Product").
		Where("user_id = ?", user.Id).
		Find(&list).Error; err != nil {
		return nil, utils.Error{
			Message:    err.Error(),
			StatusCode: http.StatusInternalServerError,
			Code:       "get_order_list_error",
		}
	}

	return list, nil
}
