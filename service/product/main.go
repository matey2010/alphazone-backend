package product

import (
	"github.com/TeplyyMaksim/alphazone_backend/db"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"github.com/TeplyyMaksim/alphazone_backend/utils"
	"gorm.io/gorm"
)

func CreateProduct(product *model.Product) error {
	if err := db.ShopDb.Create(product).Error; err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 500,
			Code:       "create_product_error",
		}
	}

	return nil
}

func GetProductList() (model.ProductList, error) {
	list := model.ProductList{}

	if err := db.ShopDb.Find(&list).Error; err != nil {
		return nil, utils.Error{
			Message:    err.Error(),
			StatusCode: 500,
			Code:       "get_product_list_error",
		}
	}

	return list, nil
}

func GetUserProductList(user *model.User) (model.ProductList, error) {
	list := model.ProductList{}

	if err := db.ShopDb.
		Where("creator_id = ?", user.Id).
		Find(&list).Error; err != nil {
		return nil, utils.Error{
			Message:    err.Error(),
			StatusCode: 500,
			Code:       "get_user_product_list_error",
		}
	}

	return list, nil
}

func UpdateProduct(product *model.Product) error {
	if err := db.ShopDb.Transaction(func(tx *gorm.DB) error {
		prevProduct := model.Product{}
		tx.First(&prevProduct, product.Id)

		if prevProduct.CreatorId != product.CreatorId {
			return utils.Error{
				Message:    "Can't update product that does not belong to you",
				StatusCode: 500,
				Code:       "product_does_not_belong_to_user_error",
			}
		}

		if err := tx.Save(product).Error; err != nil {
			return utils.Error{
				Message:    err.Error(),
				StatusCode: 500,
				Code:       "update_product_error",
			}
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

func DeleteProduct(id int) error {
	if err := db.ShopDb.Delete(&model.Product{}, id).Error; err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 500,
			Code:       "delete_product_error",
		}
	}

	return nil
}

func GetFavoriteProductList(user *model.User) (model.UserFavoriteProductList, error) {
	var list model.UserFavoriteProductList

	if err := db.ShopDb.Where("user_id = ?", user.Id).Find(&list).Error; err != nil {
		return nil, utils.Error{
			Message:    err.Error(),
			StatusCode: 500,
			Code:       "get_favorite_product_list_error",
		}
	}

	return list, nil
}

func FavoriteProduct(user *model.User, productId int) error {
	userFavoriteProduct := &model.UserFavoriteProduct{
		UserId:    user.Id,
		ProductId: productId,
	}

	if err := db.ShopDb.Create(userFavoriteProduct).Error; err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 500,
			Code:       "favorite_product_error",
		}
	}

	return nil
}

func UnfavoriteProduct(user *model.User, productId int) error {
	if err := db.ShopDb.Where("user_id = ? AND product_id = ? ", user.Id, productId).
		Delete(&model.UserFavoriteProduct{}).Error; err != nil {
		return utils.Error{
			Message:    err.Error(),
			StatusCode: 500,
			Code:       "unfavorite_product_error",
		}
	}

	return nil
}
