package utils

import (
	"github.com/TeplyyMaksim/alphazone_backend/constants"
	"github.com/TeplyyMaksim/alphazone_backend/model"
	"github.com/labstack/echo/v4"
	"net/http"
)

var parseUserError = Error{
	Message:    "Parse user error",
	StatusCode: http.StatusInternalServerError,
	Code:       "parse_user_error",
}

func GetUserFromEchoContext (c echo.Context) (*model.User, error) {
	userInterface := c.Get(constants.ContextUserKey)

	user, ok := userInterface.(*model.User)

	if !ok {
		return nil, parseUserError
	}

	return user, nil
}

func GetTokenFromEchoContext (c echo.Context) (string, error) {
	tokenInterface := c.Get(constants.ContextTokenKey)

	token, ok := tokenInterface.(string)

	if !ok {
		return "", parseUserError
	}

	return token, nil
}
