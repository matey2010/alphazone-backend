package utils

type Error struct {
	Message 		string 		`json:"message"`
	StatusCode 		int			`json:"status_code"`
	Code 			string		`json:"code"`
}
func (error Error) Error() string {
	return error.Message
}

type ErrorList []Error

func NewError(statusCode int, message string, code string) Error {
	return Error{
		StatusCode: statusCode,
		Message: message,
		Code: code,
	}
}